module gitlab.com/passelecasque/oddcheck

go 1.14

require (
	github.com/dgraph-io/badger v1.6.1
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/catastrophic/assistance v0.37.0
	gitlab.com/passelecasque/obstruction v0.14.0
	gopkg.in/yaml.v2 v2.3.0
)

// replace gitlab.com/passelecasque/obstruction => ../../passelecasque/obstruction
