# oddcheck

`ODdcheck`. Yes, I know, the extra d is odd.

This helps find which OD entries do *not* have a retail epub version available.

There are two parts: a Go binary & a ViolentMonkey user script. 

## installation

It is possible to compile this yourself using a Go environment and the Makefile. 
However the Gitlab CI generates binaries ([CI jobs artifacts -> download](https://gitlab.com/passelecasque/oddcheck/-/pipelines)) which are simpler to just download and run. A windows version is provided but not tested (and it doesn't seem to like Wine). 

Copy `test/config.yaml` to wherever you put the binary and update the `rss_key` value, as found in RSS links when searching. 

Install the `oddcheck.user.js` file in ViolentMonkey (or equivalent). 

## usage

Run the binary, then browse to any OD search page. 

### browser view

Relevant entries are visually marked, and lines are added displaying: 
- the current number of holds, the number of available copies (if not available), and the publisher.
- ISBN (if found)

![OD view](https://ptpimg.me/93v9wy.png)

### server log
The server log will also show what was found (`NF`: Not Found, `NR`: only Non-Retail found):

![Log view](https://ptpimg.me/830566.png)
