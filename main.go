package main

import (
	"fmt"

	"github.com/dgraph-io/badger"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	DefaultConfigurationFile = "config.yaml"
	dbFile                   = "oddcheck.db"
	fullName                 = "oddcheck"
)

var (
	Version = "dev"
)

func main() {
	var err error
	config, err = NewConfig(DefaultConfigurationFile)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	opt := badger.DefaultOptions(dbFile)
	opt.Logger = nil
	db, err := badger.Open(opt)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return
	}
	defer db.Close()

	// log in & search for duplicates
	b, err := tracker.NewBibliotik("https://bibliotik.me", "", "", "", config.Bibliotik.RSSKey, fullName+"/"+Version)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	// launching the websocket server
	WebSocketServer(b, db)
}
