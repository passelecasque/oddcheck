package main

import (
	"strings"
	"time"

	"github.com/dgraph-io/badger"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	separator = "◭"
)

func AddRelease(db *badger.DB, id, log string, isInteresting bool) error {
	return db.Update(func(txn *badger.Txn) error {
		var e *badger.Entry
		if isInteresting {
			// keeping the release one day only
			e = badger.NewEntry([]byte(id), []byte(log)).WithTTL(24 * time.Hour)
		} else {
			// if already on site, store indefinitely
			e = badger.NewEntry([]byte(id), []byte(log))
		}
		return txn.SetEntry(e)
	})
}

func IsKnown(db *badger.DB, ODID string) (bool, *CachedBook) {
	var data []byte
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(ODID))
		if err != nil {
			return err
		}
		data, err = item.ValueCopy(nil)
		return err
	})
	if err != nil {
		if err != badger.ErrKeyNotFound {
			logthis.Error(err, logthis.VERBOSESTEST)
			return false, nil
		}
		return false, nil
	}
	if len(data) == 0 {
		logthis.Info("found "+ODID+" but seems empty", logthis.VERBOSESTEST)
		return false, nil
	}
	parts := strings.Split(string(data), separator)
	if len(parts) != 2 {
		logthis.Info("found "+ODID+" but cannot parse data", logthis.VERBOSESTEST)
		return false, nil
	}
	cb := &CachedBook{ID: ODID, Log: parts[0], IsInteresting: isTrue(parts[1])}
	return true, cb
}

func isTrue(b string) bool {
	if strings.EqualFold(b, "true") {
		return true
	}
	return false
}
