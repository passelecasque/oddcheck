package main

type ODBooks []struct {
	AvailabilityType string `json:"availabilityType"`
	AvailableCopies  int64  `json:"availableCopies"`
	Bisac            []struct {
		Code        string `json:"code"`
		Description string `json:"description"`
	} `json:"bisac"`
	BisacCodes  []string `json:"bisacCodes"`
	Constraints struct {
		IsDisneyEulaRequired bool `json:"isDisneyEulaRequired"`
	} `json:"constraints"`
	Covers struct {
		Cover150Wide struct {
			Height       int64  `json:"height"`
			Href         string `json:"href"`
			PrimaryColor struct {
				Hex string `json:"hex"`
				Rgb struct {
					Blue  int64 `json:"blue"`
					Green int64 `json:"green"`
					Red   int64 `json:"red"`
				} `json:"rgb"`
			} `json:"primaryColor"`
			Width int64 `json:"width"`
		} `json:"cover150Wide"`
		Cover300Wide struct {
			Height       int64  `json:"height"`
			Href         string `json:"href"`
			PrimaryColor struct {
				Hex string `json:"hex"`
				Rgb struct {
					Blue  int64 `json:"blue"`
					Green int64 `json:"green"`
					Red   int64 `json:"red"`
				} `json:"rgb"`
			} `json:"primaryColor"`
			Width int64 `json:"width"`
		} `json:"cover300Wide"`
		Cover510Wide struct {
			Height       int64  `json:"height"`
			Href         string `json:"href"`
			PrimaryColor struct {
				Hex string `json:"hex"`
				Rgb struct {
					Blue  int64 `json:"blue"`
					Green int64 `json:"green"`
					Red   int64 `json:"red"`
				} `json:"rgb"`
			} `json:"primaryColor"`
			Width int64 `json:"width"`
		} `json:"cover510Wide"`
	} `json:"covers"`
	Creators []struct {
		ID   int64  `json:"id"`
		Name string `json:"name"`
		Role string `json:"role"`
	} `json:"creators"`
	Description          string `json:"description"`
	Edition              string `json:"edition"`
	EstimatedReleaseDate string `json:"estimatedReleaseDate"`
	EstimatedWaitDays    int64  `json:"estimatedWaitDays"`
	FirstCreatorID       int64  `json:"firstCreatorId"`
	FirstCreatorName     string `json:"firstCreatorName"`
	Formats              []struct {
		BundledContent           []interface{} `json:"bundledContent"`
		FileSize                 int64         `json:"fileSize"`
		HasAudioSynchronizedText bool          `json:"hasAudioSynchronizedText"`
		ID                       string        `json:"id"`
		Identifiers              []struct {
			Type  string `json:"type"`
			Value string `json:"value"`
		} `json:"identifiers"`
		IsBundleParent bool          `json:"isBundleParent"`
		Isbn           string        `json:"isbn"`
		Name           string        `json:"name"`
		OnSaleDateUtc  string        `json:"onSaleDateUtc"`
		Rights         []interface{} `json:"rights"`
	} `json:"formats"`
	HoldsCount int64  `json:"holdsCount"`
	HoldsRatio int64  `json:"holdsRatio"`
	ID         string `json:"id"`
	Imprint    struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"imprint"`
	IsAvailable              bool `json:"isAvailable"`
	IsBundledChild           bool `json:"isBundledChild"`
	IsFastlane               bool `json:"isFastlane"`
	IsHoldable               bool `json:"isHoldable"`
	IsOwned                  bool `json:"isOwned"`
	IsPreReleaseTitle        bool `json:"isPreReleaseTitle"`
	IsRecommendableToLibrary bool `json:"isRecommendableToLibrary"`
	JuvenileEligible         bool `json:"juvenileEligible"`
	Languages                []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"languages"`
	Levels []struct {
		High  string `json:"high"`
		ID    string `json:"id"`
		Low   string `json:"low"`
		Name  string `json:"name"`
		Value string `json:"value"`
	} `json:"levels"`
	LuckyDayAvailableCopies int64  `json:"luckyDayAvailableCopies"`
	LuckyDayOwnedCopies     int64  `json:"luckyDayOwnedCopies"`
	OwnedCopies             int64  `json:"ownedCopies"`
	PublishDate             string `json:"publishDate"`
	PublishDateText         string `json:"publishDateText"`
	Publisher               struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"publisher"`
	PublisherAccount struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"publisherAccount"`
	Ratings struct {
		MaturityLevel struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"maturityLevel"`
		NaughtyScore struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"naughtyScore"`
	} `json:"ratings"`
	ReserveID    string `json:"reserveId"`
	ReviewCounts struct {
		Premium           int64 `json:"premium"`
		PublisherSupplier int64 `json:"publisherSupplier"`
	} `json:"reviewCounts"`
	Sample struct {
		Href string `json:"href"`
	} `json:"sample"`
	Series          string  `json:"series"`
	SortTitle       string  `json:"sortTitle"`
	StarRating      float32 `json:"starRating"`
	StarRatingCount int64   `json:"starRatingCount"`
	Subjects        []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"subjects"`
	Subtitle string `json:"subtitle"`
	Title    string `json:"title"`
	Type     struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"type"`
	VisitorEligible    bool `json:"visitorEligible"`
	YoungAdultEligible bool `json:"youngAdultEligible"`
}
