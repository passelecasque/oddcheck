package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"github.com/dgraph-io/badger"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	WebsocketPort    = 8180
	HandshakeCommand = "hello"
	SearchCommand    = "search"
	TagCommand       = "tag"

	errorIncomingWebSocketJSON = "Error parsing websocket input"
	errorOutgoingWebSocketJSON = "Error writing to websocket"
)

const (
	responseInfo = iota
	responseError
)

// IncomingJSON from the websocket created by the GM script.
type IncomingJSON struct {
	Command string
	Args    string
	Site    string
}

// OutgoingJSON to the websocket created by the GM script.
type OutgoingJSON struct {
	Status  int
	Command string
	ID      string
	Info    string
	Info2   string
}

type CachedBook struct {
	ID            string
	Log           string
	IsInteresting bool
}

var (
	ErrorCreatingWebSocket = errors.New("error creating websocket")
)

func WebSocketServer(b *tracker.Bibliotik, db *badger.DB) {
	rtr := mux.NewRouter()
	var mutex = &sync.Mutex{}

	upgrader := websocket.Upgrader{
		// allows connection to websocket from anywhere
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	socket := func(w http.ResponseWriter, r *http.Request) {
		c, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			logthis.Error(fmt.Errorf("%q: %w", ErrorCreatingWebSocket, err), logthis.NORMAL)
			return
		}
		defer c.Close()
		// channel to know when the connection with a specific instance is over
		endThisConnection := make(chan struct{})

		for {
			// TODO if server is shutting down, c.Close()
			incoming := IncomingJSON{}
			if err := c.ReadJSON(&incoming); err != nil {
				if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					logthis.Error(errors.Wrap(err, errorIncomingWebSocketJSON), logthis.VERBOSEST)
				}
				endThisConnection <- struct{}{}
				break
			}

			var answer OutgoingJSON
			// dealing with command
			switch incoming.Command {
			case HandshakeCommand:
				// say hello right back
				answer = OutgoingJSON{Status: responseInfo, Command: HandshakeCommand}
				// writing answer
				mutex.Lock()
				if err := c.WriteJSON(answer); err != nil {
					if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
						logthis.Error(errors.Wrap(err, errorOutgoingWebSocketJSON), logthis.VERBOSEST)
					}
					endThisConnection <- struct{}{}
					break
				}
				mutex.Unlock()

			case SearchCommand:
				jsonString := strings.Replace(incoming.Args[:len(incoming.Args)-1], "window.OverDrive.titleCollection = ", "", 1)
				var books ODBooks

				//fmt.Println(jsonString)

				if err := json.Unmarshal([]byte(jsonString), &books); err != nil {
					logthis.TimedError(err, logthis.NORMAL)
					return
				}

				for _, od := range books {
					var foundEpub, foundAudiobook bool
					var ISBNs []string
					// checking formats and collecting ISBNs for epubs
					for _, f := range od.Formats {
						if strings.EqualFold(f.Name, "EPUB eBook") || strings.EqualFold(f.Name, "Open EPUB eBook") {
							foundEpub = true
							ISBNs = append(ISBNs, f.Isbn)
						}
						if strings.EqualFold(f.Name, "OverDrive Listen audiobook") || strings.EqualFold(f.Name, "MP3 audiobook") {
							foundAudiobook = true
						}
					}
					strslice.RemoveDuplicates(&ISBNs)

					if !foundEpub && (!od.IsPreReleaseTitle || (od.IsPreReleaseTitle && foundAudiobook)) {
						// not considering books without EPUB versions
						// some pre-release books have no formats yet
						// not considering pre-release audiobooks
						continue
					}

					// preparing metadata
					var authors []string
					for _, author := range od.Creators {
						if author.Role == "Author" || author.Role == "Editor" {
							authors = append(authors, author.Name)
						}
					}
					var subjects []string
					for _, sub := range od.Subjects {
						subjects = append(subjects, sub.Name)
					}
					odSubjects := "[" + strings.Join(subjects, ", ") + "]"

					publisher := od.Publisher.Name
					if len(authors) == 1 && strings.EqualFold(authors[0], publisher) {
						publisher = "Self-published"
					}
					odPublisher := " (" + publisher + ")"
					var info string
					if !od.IsAvailable {
						info = fmt.Sprintf("[%d/%d] %s", od.HoldsCount, od.OwnedCopies, publisher)
					} else {
						info = publisher
					}
					url := fmt.Sprintf("https://%s/media/%s", incoming.Site, od.ID)
					language := "English"
					if len(od.Languages) != 0 {
						language = od.Languages[0].Name
					}

					// checking if is in cache
					var isInteresting bool
					mutex.Lock()
					isKnown, cachedBook := IsKnown(db, od.ID)
					mutex.Unlock()

					if isKnown {
						if cachedBook.IsInteresting {
							isInteresting = true
							logthis.TimedInfo(cachedBook.Log+" (cached)", logthis.NORMAL)
							answer = OutgoingJSON{Status: responseInfo, Command: TagCommand, ID: cachedBook.ID, Info: info, Info2: strings.Join(ISBNs, "/")}
						}
					} else {
						// if not in cache, searching
						// else searching tracker
						var hits []*tracker.Book
						hits, err = b.SearchBooksByRSS(strings.Join(authors, ","), od.Title)
						if err != nil {
							logthis.TimedError(err, logthis.NORMAL)
							return
						}

						// checking hits
						var log string
						if len(hits) != 0 {
							// showing if has at least one NON-RETAIL
							for _, h := range hits {
								if !h.IsRetail && language == h.Language {
									isInteresting = true
									log = ui.Yellow(fmt.Sprintf("NR | %s | %s - %s ", url, strings.Join(authors, ","), od.Title)) + odSubjects + ui.BlueBold(odPublisher)
									break
								}
							}
						} else {
							isInteresting = true
							var preRelease string
							if od.IsPreReleaseTitle {
								preRelease = "(pre-release) "
							}
							log = ui.GreenBold(fmt.Sprintf("NF | %s | %s%s - %s ", url, preRelease, strings.Join(authors, ","), od.Title)) + odSubjects + ui.BlueBold(odPublisher)
						}

						// adding in cache
						mutex.Lock()
						desc := fmt.Sprintf("%s%s%v", log, separator, isInteresting)
						if err := AddRelease(db, od.ID, desc, isInteresting); err != nil {
							logthis.Error(err, logthis.NORMAL)
						}
						mutex.Unlock()
						if isInteresting {
							logthis.TimedInfo(log, logthis.NORMAL)
							answer = OutgoingJSON{Status: responseInfo, Command: TagCommand, ID: od.ID, Info: info, Info2: strings.Join(ISBNs, "/")}
						}
					}

					if isInteresting {
						// writing answer
						mutex.Lock()
						if err := c.WriteJSON(answer); err != nil {
							if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
								logthis.Error(errors.Wrap(err, errorOutgoingWebSocketJSON), logthis.VERBOSEST)
							}
							endThisConnection <- struct{}{}
							break
						}
						mutex.Unlock()
					}
				}
			}
		}
	}

	rtr.HandleFunc("/ws", socket)

	// serve
	logthis.TimedInfo("Launching up websocket server.", logthis.NORMAL)
	httpServer := &http.Server{Addr: fmt.Sprintf(":%d", WebsocketPort), Handler: rtr}
	if err := httpServer.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			logthis.Info("Closing websocker server...", logthis.NORMAL)
		} else {
			logthis.Error(errors.Wrap(err, "error with websocket server"), logthis.NORMAL)
		}
	}
}
