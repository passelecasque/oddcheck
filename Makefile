GO = GO111MODULE=on go
VERSION=`git describe --tags`

all: fmt check test-coverage build

prepare:
	${GO} get -u github.com/divan/depscheck
	${GO} get github.com/warmans/golocc
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.27.0

deps:
	${GO} mod download

fmt:
	${GO} fmt ./...

check: fmt
	golangci-lint run

info: fmt
	depscheck -totalonly -tests .
	golocc .

test-coverage:
	${GO} test -race -coverprofile=coverage.txt -covermode=atomic ./...

clean:
	rm -f oddcheck
	rm -f oddcheck_windows.exe
	rm -f oddcheck_darwin
	rm -f coverage.txt

build:
	${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o oddcheck
	GOOS=windows GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o oddcheck_windows.exe
	GOOS=darwin GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o oddcheck_darwin

install:
	${GO} install -trimpath -ldflags "-X main.Version=${VERSION}"




