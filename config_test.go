package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	fmt.Println("+ Testing Config...")
	check := assert.New(t)

	// setting up
	c := &Config{}
	err := c.Load("test/config.yaml")
	check.Nil(err)

	// general
	fmt.Println("Checking general")
	check.Equal(3, c.General.LogLevel)

	// trackers
	fmt.Println("Checking trackers")
	check.Equal("rss_key_found_in_html_source", c.Bibliotik.RSSKey)

	fmt.Println(c.String())
}
