package main

import (
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

type ConfigGeneral struct {
	LogLevel int `yaml:"log_level"`
}

func (cg *ConfigGeneral) check() error {
	return logthis.CheckLevel(cg.LogLevel)
}

// String representation for ConfigGeneral.
func (cg *ConfigGeneral) String() string {
	txt := "General configuration:\n"
	txt += "\tLog level: " + strconv.Itoa(cg.LogLevel) + "\n"
	return txt
}

type ConfigBib struct {
	RSSKey string `yaml:"rss_key"`
}

func (cb *ConfigBib) String() string {
	txt := "Tracker configuration:\n"
	txt += "\tRSS Key: " + cb.RSSKey + "\n"
	return txt
}

func (cb *ConfigBib) check() error {
	if cb.RSSKey == "" {
		return errors.New("RSS key must be provided, it can be found in HTML source (ctrl+U)")
	}
	return nil
}
