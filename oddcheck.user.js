// ==UserScript==
// @name           oddcheck
// @namespace      oddcheck
// @description    checks bib for OD things
// @include        https://*.overdrive.com/search*
// @include        https://*.overdrive.com/*/search*
// @version        10
// @date           2020-06
// @grant          GM.getValue
// @grant          GM.setValue
// @grant          GM.notification
// ==/UserScript==

(async function () {
	let isWebSocketConnected = false;
	let sock;
	let hello;

	console.log("script loaded")
	hello = {
		Command: 'hello',
	};
	newSocket();

	function newSocket() {
		console.log('setting up socket');
		sock = new WebSocket('ws://127.0.0.1:8180/ws');

		sock.onopen = function () {
			console.log('Connected to the server');
			isWebSocketConnected = true;
			// Send the msg object as a JSON-formatted string.
			sock.send(JSON.stringify(hello));
		};
		sock.onerror = () => {
			console.log('Websocket error.');
			isWebSocketConnected = false;
		};
		sock.onmessage = function (evt) {
			console.log('Received something from socket');
			const msg = JSON.parse(evt.data);
			if (msg.Status === 0) {
				if (msg.Command === 'hello') {
					console.log("handshake completed.")

					console.log("getting book info");
					// everything is in a JSON in a <script> tag.
					// finding that JSON and sending it back to Go for parsing
					var scripts = document.getElementsByTagName("script")
					for (var i = 0; i < scripts.length; ++i) {
						let lines = scripts[i].innerHTML.split('\n');
						for (var j = 0; j < lines.length; j++) {
							if (lines[j].trim().startsWith("window.OverDrive.titleCollection")) {
								const search = {
									Command: 'search',
									Site: window.location.host,
									Args: lines[j].trim()
								};
								sock.send(JSON.stringify(search));
								break
							}
						}
					}
				} else if (msg.Command === 'tag') {
					console.log("marking media "+ msg.ID + " as interesting.");
					prependTitle(msg.ID, msg.Info, msg.Info2);
				} else {
					console.log(evt.data);
				}
			}
		};
		sock.onclose = function () {
			console.log('Server connection closed.');
		};
	}

	function prependTitle(id, info, info2) {
		let titles = document.querySelectorAll('h3[data-media-id] > a');
		for (let i = 0; i < titles.length; ++i) {
			let href = titles[i].getAttribute("href")
			if (href.endsWith(id) ){
				addText(titles[i], info);
				addText(titles[i], info2);
				let card_border = titles[i].parentNode.parentNode.parentNode.parentNode.parentNode;
				card_border.style.backgroundColor = "#ff4545";
			}
		}
	}

	function addText(startNode, text) {
		// cloning the title node, setting the text as additional info, and appending to end of book card
		// this is making me feel dirty. there has to be a cleaner way to do this.
		// h3 > A. cloning A, modifying. Cloning h3, blanking, setting new A as child. Appending the new h3 to parent node.
		let title_copy = startNode.cloneNode(true);
		title_copy.innerHTML = text;
		title_copy.setAttribute("href", "#");

		let parent_copy = startNode.parentNode.cloneNode(true);
		parent_copy.innerHTML = '';
		parent_copy.appendChild(title_copy);

		let grand_parent = startNode.parentElement.parentElement;
		grand_parent.appendChild(parent_copy);
	}
})();
